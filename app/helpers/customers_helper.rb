module CustomersHelper
  def extract_params_from_email(text)
    return {
      name: text.string_between_markers('Name : ', '\r'),
      email: text.string_between_markers('Email : ', '\r'),
      phone: text.string_between_markers('Phone : ', '\r'),
      phone_ext: text.string_between_markers('EXT : ', '\r'),
      
      house_size: text.string_between_markers('Bedrooms : ', '\r'),
      distance: text.string_between_markers('Distance : ', '\r'),
      move_date: text.match(/^Move Date : (.*)/) ? text.match(/^Move Date : (.*)/)[1] : nil,
      
      from_areacode: text.string_between_markers('From Areacode : ', '\r'),
      from_city: text.string_between_markers('From City : ', '\r'),
      from_state: text.string_between_markers('From State : ', '\r'),
      from_zip: text.string_between_markers('From Zip : ', '\r'),
      
      to_areacode: text.string_between_markers('To Areacode : ', '\r'),
      to_city: text.string_between_markers('To City : ', '\r'),
      to_state: text.string_between_markers('To State : ', '\r'),
      to_zip: text.string_between_markers('To Zip : ', '\r')
    }
  end
end