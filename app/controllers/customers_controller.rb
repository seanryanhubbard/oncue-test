class CustomersController < ApplicationController
  include CustomersHelper
  def index
    @customers = Customer.search(params[:search])
  end

  def new
    @customer = Customer.new
  end

  def create
    email = params[:email]
    customer_params = extract_params_from_email(email)
    puts customer_params
    
    customer = Customer.new(customer_params)
    
    if customer.save
      redirect_to root_path
    else
      render 'new'
    end
  end
end
