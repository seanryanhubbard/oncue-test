class String
  def string_between_markers(marker1, marker2)
    self.split(/#{marker1}(.*?)#{marker2}/)[1]
  end
end