class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :phone_ext
      t.string :from_areacode
      t.string :from_zip
      t.string :from_state
      t.string :from_city
      t.string :to_areacode
      t.string :to_zip
      t.string :to_state
      t.string :to_city
      t.integer :house_size
      t.date :move_date
      t.decimal :distance

      t.timestamps
    end
  end
end
